import React, {useState, useContext} from "react";
import {useHttp} from '../../hooks/http.hook'
import {useHistory} from 'react-router-dom'
import {AuthContext} from '../../context/AuthContext'

export const CreatePage = () => {
    const [link, setLink] = useState('')
    const {request} = useHttp()
    const auth = useContext(AuthContext)
    const history = useHistory()

    const pressHandler = async e => {
        if (e.key === 'Enter') {
            try {
                const data = await request('/api/link/generate', 'POST', {from: link}, {
                    Authorization: `Bearer ${auth.token}`
                })
                history.push(`/detail/${data.link._id}`)
            } catch (e) {
                
            }
        }
    }

  return (
    <div className="row">
      <div className="col s8 offset-s2">
        <div className="input-field">
          <input
            id="email"
            type="text"
            className="validate"
            onChange={e => setLink(e.target.value)}
            value={link}
            onKeyPress={pressHandler}
          />
          <label htmlFor="email">Link</label>
        </div>
      </div>
    </div>
  );
};
