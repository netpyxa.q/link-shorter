import React, {useState, useCallback, useContext, useEffect} from "react";
import {useParams} from 'react-router-dom'
import {useHttp} from '../../hooks/http.hook'
import {AuthContext} from '../../context/AuthContext'
import { Loader } from "../Loader/Loader";
import { LinkCard } from "../LinkCard/LinkCard";

export const DetailPage = () => {
    const [link, setLink] = useState()
    const {token} = useContext(AuthContext)
    const linkId = useParams().id
    const {request, loading} = useHttp()

    const getLink = useCallback(async() => {
        try {
            const fetched = await request(`/api/link/${linkId}`, 'GET', null, {
                Authorization: `Bearer ${token}`
            })
            setLink(fetched)
        } catch (error) {
            
        }
    }, [token, linkId, request])

    useEffect(() => {
        getLink()
    }, [getLink])

    if (loading) {
        return <Loader/>
    }

    return (
        <React.Fragment>
            { !loading && link && <LinkCard link={link} />}
        </React.Fragment>
    )
}