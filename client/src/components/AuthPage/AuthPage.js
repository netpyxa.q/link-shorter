import React, { useState, useEffect, useContext } from "react";
import { useHttp } from '../../hooks/http.hook'
import { useMessage } from '../../hooks/message.hook'
import { AuthContext } from "../../context/AuthContext";

export const AuthPage = () => {
    const auth = useContext(AuthContext)
    const message = useMessage()
    const { loading, error, request, clearError } = useHttp()
    const [form, setFrom] = useState({
        email: '', password: ''
    })

    useEffect(() => {
        message(error)
        clearError()
    }, [error, message, clearError])
    
    const changeHandler = e => {
        setFrom({ ...form, [e.target.name]: e.target.value})
    }

    const registerHandler = async () => {
        try {
            const data = await request('/api/auth/registration', 'POST', {...form})
            console.log(data)
        } catch (e) {
            
        }
    }

    const loginHandler = async () => {
        try {
            const data = await request('/api/auth/login', 'POST', {...form})
            auth.login(data.token, data.userId)
        } catch (e) {
            
        }
    }

    return (
        <div className="row">
            <div className="col s6 offset-s3">
                <h1>
                    Sokrati link
                </h1>
                <div className="card blue darken-1">
                    <div className="card-content white-text">
                        <span className="card-title">Auth</span>
                        <div>

                            <div className="input-field">
                                <input 
                                    id="email" 
                                    type="email" 
                                    className="validate"  
                                    placeholder="Enter email"  
                                    name="email"
                                    onChange={changeHandler}
                                    value={form.email}
                                />
                                <label htmlFor ="email">Email</label>
                            </div>
                            
                            <div className="input-field">
                                <input 
                                    id="password" 
                                    type="password" 
                                    name="password"
                                    placeholder="Enter password"  
                                    onChange={changeHandler}
                                    value={form.password}
                                />
                                <label htmlFor ="password">Password</label>
                            </div>

                        </div>
                    </div>
                    <div className="card-action">
                        <button 
                            className="btn yellow darken-4"
                            onClick={loginHandler}
                            disabled={loading}
                        >
                            Sign in
                        </button>
                        <button 
                            className="btn grey lighten-1 black-text"
                            onClick={registerHandler}
                            disabled={loading}
                        >
                            Register
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}